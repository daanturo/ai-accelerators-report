\contentsline {chapter}{\numberline {1}Giới thiệu về AI accelerator}{5}{chapter.1}% 
\contentsline {chapter}{\numberline {2}Một số processor chuyên dụng để tăng tốc cho AI/ML}{7}{chapter.2}% 
\contentsline {section}{\numberline {2.1}APPLE A12 \& A13 BIONIC CHIP}{7}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}Giới thiệu}{7}{subsection.2.1.1}% 
\contentsline {subsection}{\numberline {2.1.2}Cấu tạo}{8}{subsection.2.1.2}% 
\contentsline {subsection}{\numberline {2.1.3}Tăng tốc độ cho AI/ML}{10}{subsection.2.1.3}% 
\contentsline {subsubsection}{\numberline {2.1.3.1}Về GPU}{10}{subsubsection.2.1.3.1}% 
\contentsline {subsubsection}{\numberline {2.1.3.2}Về Neural Engine}{11}{subsubsection.2.1.3.2}% 
\contentsline {subsubsection}{\numberline {2.1.3.3}Về CPU}{11}{subsubsection.2.1.3.3}% 
\contentsline {subsubsection}{\numberline {2.1.3.4}Kết luận: So sánh tổng quát Apple A13 Bionic với Apple A12 Bionic}{12}{subsubsection.2.1.3.4}% 
\contentsline {section}{\numberline {2.2}Intel Nervana Neural Network Processor}{12}{section.2.2}% 
\contentsline {subsection}{\numberline {2.2.1}Tổng quan}{12}{subsection.2.2.1}% 
\contentsline {subsection}{\numberline {2.2.2}Intel NNP-T 1000 (Spring Crest)}{13}{subsection.2.2.2}% 
\contentsline {subsubsection}{\numberline {2.2.2.1}Thiết kế Spring Crest}{13}{subsubsection.2.2.2.1}% 
\contentsline {subsubsection}{\numberline {2.2.2.2}Hiệu năng}{14}{subsubsection.2.2.2.2}% 
\contentsline {subsection}{\numberline {2.2.3}Intel NNP-I 1000 (Spring Hill)}{15}{subsection.2.2.3}% 
\contentsline {subsubsection}{\numberline {2.2.3.1}Thiết kế Spring Hill}{15}{subsubsection.2.2.3.1}% 
\contentsline {subsubsection}{\numberline {2.2.3.2}Hiệu năng}{17}{subsubsection.2.2.3.2}% 
\contentsline {section}{\numberline {2.3}NVIDIA TESLA K Series}{17}{section.2.3}% 
\contentsline {subsection}{\numberline {2.3.1}Lợi thế của GPU so với CPU trong việc tăng tốc AI/ML}{17}{subsection.2.3.1}% 
\contentsline {subsection}{\numberline {2.3.2}NVIDIA Tesla}{17}{subsection.2.3.2}% 
\contentsline {subsection}{\numberline {2.3.3}Kiến trúc phần cứng V100 GPU}{18}{subsection.2.3.3}% 
\contentsline {subsection}{\numberline {2.3.4}Volta Streaming Multiprocessor}{18}{subsection.2.3.4}% 
\contentsline {subsection}{\numberline {2.3.5}Tensor Cores}{19}{subsection.2.3.5}% 
\contentsline {section}{\numberline {2.4}Tensor Processing Unit}{20}{section.2.4}% 
\contentsline {subsection}{\numberline {2.4.1}Sơ lược}{21}{subsection.2.4.1}% 
\contentsline {subsection}{\numberline {2.4.2}Các thế hệ TPU}{22}{subsection.2.4.2}% 
\contentsline {subsubsection}{\numberline {2.4.2.1}Thế hệ đầu tiên}{22}{subsubsection.2.4.2.1}% 
\contentsline {subsubsection}{\numberline {2.4.2.2}Thế hệ thứ hai}{23}{subsubsection.2.4.2.2}% 
\contentsline {subsubsection}{\numberline {2.4.2.3}Thế hệ thứ ba}{24}{subsubsection.2.4.2.3}% 
\contentsline {subsubsection}{\numberline {2.4.2.4}Edge TPU}{24}{subsubsection.2.4.2.4}% 
